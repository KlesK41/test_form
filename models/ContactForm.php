<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $date;
    public $time;

    public $enableDays = [
        'Пн' => 1,
        'Вт' => 2,
        'Ср' => 3,
        'Чт' => 4
    ];

    private $enableHours = [
        'start' => '08:00',
        'end' => '18:00'
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'date', 'time'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            [['date', 'time'], 'safe'],
            ['date', 'validateDate', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['time', 'validateTime', 'skipOnEmpty' => false, 'skipOnError' => false]

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    public function validateDate($attribute) {
        if (!in_array($this->$attribute, $this->enableDays)) {
            $this->addError($attribute, 'Wrong day');
        }
    }

    public function validateTime($attribute) {
        if (strtotime($this->$attribute) < strtotime($this->enableHours['start'])
            || strtotime($this->$attribute) > strtotime($this->enableHours['end'])) {
            $this->addError($attribute, 'Closed');
        }
    }

}
