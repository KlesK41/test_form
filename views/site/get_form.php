<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-5">

                <form id="contact-form" method="post" role="form" class="ng-pristine ng-valid"
                      name="form"
                      ng-submit="form.$valid && sendForm(formValues, form)"
                      novalidate
                >

                    <div class="form-group field-contactform-name required"
                         ng-class="{
                            'has-error' : form.name.$error.required &&  (form.$submitted || form.name.$touched),
                            'has-success' : !form.name.$error.required
                        }"
                    >
                        <label class="control-label" for="contactform-name">Name</label>
                        <input type="text" id="contactform-name" class="form-control" name="name" ng-model="formValues.name" required>
                        <div ng-show="(form.$submitted || form.name.$touched)">
                            <p class="help-block help-block-error" ng-show="form.name.$error.required">
                                Name cannot be blank.
                            </p>
                        </div>
                    </div>

                    <div class="form-group field-contactform-email required"
                         ng-class="{
                            'has-error' : (form.email.$error.required || form.email.$error.email) && (form.$submitted || form.email.$touched),
                            'has-success' : !(form.email.$error.required || form.email.$error.email)
                        }"
                    >
                        <label class="control-label" for="contactform-email">Email</label>
                        <input type="email" id="contactform-email" class="form-control" name="email" ng-model="formValues.email"
                               required
                        >
                        <div ng-show="(form.$submitted || form.email.$touched)">
                            <p ng-show="form.email.$error.required" class="help-block help-block-error" >Email cannot be blank.</p>
                            <div ng-show="form.email.$error.email" class="help-block">Email is not a valid email address.</div>
                        </div>
                    </div>

                    <div class="form-group field-contactform-name required"
                         ng-class="{
                            'has-error' : (form.date.$error.required &&  (form.$submitted || form.date.$touched)) || errors.date,
                            'has-success' : !form.date.$error.required
                        }"
                    >
                        <label class="control-label" for="contactform-name">Date</label>
                        <input type="date" id="contactform-date" class="form-control" name="date" ng-model="formValues.date" required>
                        <div ng-show="(form.$submitted || form.date.$touched)">
                            <p ng-show="form.date.$error.required" class="help-block help-block-error">Date cannot be blank.</p>
                            <p ng-show="errors.date" class="help-block help-block-error" ng-repeat="dateError in errors.date">{{ dateError }}</p>
                        </div>
                    </div>

                    <div class="form-group field-contactform-name required"
                         ng-class="{
                            'has-error' : (form.time.$error.required &&  (form.$submitted || form.time.$touched)) || !!errors.time,
                            'has-success' : !form.time.$error.required
                        }"
                    >
                        <label class="control-label" for="contactform-name">Time</label>
                        <input type="time" id="contactform-time" class="form-control" name="time" ng-model="formValues.time" required>
                        <div ng-show="(form.$submitted || form.time.$touched)">
                            <p ng-show="form.time.$error.required" class="help-block help-block-error">Name cannot be blank.</p>
                            <p ng-show="errors.time" class="help-block help-block-error" ng-repeat="timeError in errors.time">{{ timeError }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form>

                {{ result }}

            </div>

        </div>

    </div>
</div>

