'use strict';

angular.module("app", [
    'ui.bootstrap',
    'FormServiceModule'
]);

angular.module('app')
.controller('appController', [
    '$scope',
    'FormService',
    function appController($scope, FormService) {

        function sendForm(formValues, form) {
            var data = angular.copy(formValues);
            data.time = form.time.$viewValue;
            $scope.result = '';
            FormService.sendForm(data).then(function(data) {
                $scope.errors = {};
                $scope.result = data;

                $scope.formValues = {};
                form.$setPristine();
                form.$setUntouched();
            }, function (error) {
                var errors = JSON.parse(error.data.message);
                if (!!errors)
                    $scope.errors = JSON.parse(error.data.message);
            });
        }

        $scope.formValues = {};
        $scope.errors = {};
        $scope.result = '';

        $scope.sendForm = sendForm;
    }
]);