'use strict';

angular.module('FormServiceModule', [])
    .factory("FormService", function FormServiceFactory($http) {
        var service = {};

        function sendForm(formValues) {
            return $http({
                method: 'POST',
                url: '/site/send_form',
                data: formValues,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).then(function(result) {
                return result.data.data;
            });
        }

        service.sendForm = sendForm;

        return service;
    });
