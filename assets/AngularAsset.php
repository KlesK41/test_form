<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.07.16
 * Time: 17:42
 */

namespace app\assets;


use yii\web\AssetBundle;

class AngularAsset extends AssetBundle
{
    public $sourcePath = '@bower';

    public $css = [];
    public $js = [
        'angular/angular.js',
        'angular-bootstrap/ui-bootstrap-tpls.min.js'
    ];
}